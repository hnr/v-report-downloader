
# Copyright (c) 2017 HH Partners, Attorneys-at-law Ltd
# SPDX-License-Identifier: Apache-2.0

'''
Proof-of-concept script for logging in and downloading report documents from the Validos 
report database. For now, requires a ready-made list of working download URLs.

For this to work, you need to:
- install dependencies (if not installed): bs4, requests
- create credentials.py containing your Validos.org credentials:
    username = 'YOURUSERNAME'
    password = 'YOURPASSWORD'

'''

from credentials import username, password
import cgi
import os
import sys
from bs4 import BeautifulSoup
import requests


#   Settings

target_directory = "reports"    # Path to target directory for downloads
fail_quota = 3                  # How many times has to fail (initially) before aborting

#   Starting from example URLs for now:

urls = [
    'http://validos.org/en/tietopankki/use-instructions-for-oss-packages/1985-python-cinderclient-1-1-2-validos-use-instructions-923-20160202-doc/file',
    'http://www.validos.org/fi/tietopankki/use-instructions-for-oss-packages/2356-classnames-2-2-5-validos-use-instructions-1071-20161125-doc/file',
    'http://www.validos.org/fi/tietopankki/use-instructions-for-oss-packages/2274-swagger-annotations-2-10-1-3-0-validos-use-instructions-1039-20160912-doc/file',
    'http://www.validos.org/en/tietopankki/doc_download/734-httpclient-4-2-2-validos-use-instructions-481-20121126',
    'http://www.validos.org/en/tietopankki/doc_download/896-orca-2-25-validos-use-instructions-16-20081201',
    'http://www.validos.org/en/tietopankki/doc_download/954-slf4j-1-5-5-validos-use-instructions-129-20091221'
]

#   Preventing a weird session-related issue:
urls = [u.replace('http://validos.org', 'http://www.validos.org') for u in urls]

s = requests.session()  # maintains the session after logging in

'''
Below, we get the HTML for the login view and search in the contents for the changing 
values we need for making the login POST request. Example of what we're looking for:

<input type="hidden" name="return" value="aW5kZXgucGhwP29wdGlvbj1jb21fdXNlcnMmdmlldz1wcm9maWxl" />
<input type="hidden" name="f651b6b46dfeaac302d0b2c40c696fe0" value="1" />

The criteria we use for finding these is that they are the last and second-to-last
<input> tags directly under the (only) <fieldset> tag in the document. Obviously,
if this changes (as a result of e.g. a Joomla update), the script breaks.

'''

try:
    response = s.get('http://www.validos.org/fi/component/users/?view=login')
    content = response.content
    soup = BeautifulSoup(content, 'html.parser')
    ids = soup.select('fieldset > input')
except requests.exceptions.RequestException as e:
    sys.exit("FATAL: Request error: " + str(e))

if len(ids) < 2:
    sys.exit("FATAL: Could not parse the HTML on the login page.")

login_data = {
    'username': username,
    'password': password,
    'return': ids[-2]['value'],
    ids[-1]['name']: '1'
}

#   Making the POST request in order to log in:

s.post('http://www.validos.org/fi/component/users/?task=user.login', login_data)

#   Create the target directory if it doesn't exist:

target_directory = os.path.abspath(target_directory)

try:
    os.makedirs(target_directory, exist_ok=True)
except Exception as e:
    sys.exit("FATAL: Could not create directory: " + target_directory + " (" + str(e) + ")")

#   Go through the URLs:

fail_counter = 0
counter_on = True    # If succeeds at least once, stops counting

for url in urls:

    short_url = url.replace('http://www.validos.org', '')

    if fail_counter < fail_quota:

        print("Requesting: " + short_url, end="", flush=True)

        try:
            r = s.get(url, stream=True)
        except requests.exceptions.RequestException as e:
            print("...Failed (Request failed)")
            #   Improve: implement logging for e?
            if counter_on:
                fail_counter += 1
            continue

        headers = r.headers

        if not headers['content-type'] or 'application/msword' not in headers['content-type']:
            print("...Failed (did not return Word file?)")
            if counter_on:
                fail_counter += 1
        else:
            disp = headers['content-disposition']   # Get file name info from headers
            value, params = cgi.parse_header(disp)
            file_name = params['filename']
            path = target_directory + '/' + file_name
            try:
                with open(path, 'wb') as f:
                    f.write(r.content)
            except Exception as e:
                sys.exit("FATAL: could not write to: " + path + " (" + str(e) + ")")
            print("...Success.")
            counter_on = False
    else:
        sys.exit("Fail quota reached, aborted!")
